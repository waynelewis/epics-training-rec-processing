#!../../bin/darwin-x86/recproc

## You may have to change recproc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

epicsEnvSet("PREFIX", "RecScanTest:")

## Register all support components
dbLoadDatabase "dbd/recproc.dbd"
recproc_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/count.db","PREFIX=$(PREFIX)")
dbLoadRecords("db/flnk.db","PREFIX=$(PREFIX)")
dbLoadRecords("db/inlink.db","PREFIX=$(PREFIX)")
dbLoadRecords("db/inlink_cpp.db","PREFIX=$(PREFIX)")
dbLoadRecords("db/outlink.db","PREFIX=$(PREFIX)")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=wayne"
